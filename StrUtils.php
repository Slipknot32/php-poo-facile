<?php


/**
 * 
 */
class StrUtils{

	private $str;

	public function __construct($newstr)
	{
		$this->str = $newstr;
	}

	public function bold(){
		return '<strong>'.$this->str.'</strong>'.'<br>';
	}

	public function italic(){
		return '<em>'.$this->str.'</em>'.'<br>';
	}

	public function underline(){
		return '<u>'.$this->str.'</u>'.'<br>';
	}

	public function capitalize(){
		return '<span style="text-transform:uppercase">'.$this->str.'</span>'.'<br>';
	}


	public function uglify(){
		return
		'<strong>'.
		'<em>'.
		'<u>'.
		$this->str
		.'</u>'
		.'</em>'
		.'</strong>';

		/////////////////////////////////// autre solution 
		// $this->str = $this->bold();
		// $this->str = $this->italic();
		// $this->str = $this->underline();
		// return $this->str;

	}

}



?>
